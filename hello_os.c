#include <linux/module.h>    // for all kernel modules
#include <linux/kernel.h>    // for KERN_INFO
#include <linux/init.h>      // for __init and __exit macros

// Designed by Mohsen naghipourfar && Alireza Vezvaei

MODULE_LICENSE("Mohsen/Alireza");       // set licence of module
MODULE_AUTHOR("Mohsen Naghipourfar && Alireza Vezvaei");    // authors of modules
MODULE_DESCRIPTION("A Module which just says hello to the OS!");    // module description


static int __init hello_OS_init(void) {     // init function for hello_os module
    printk(KERN_INFO "Hello Dear OS! This is a module created by Mohsen && Alireza! :)) \n");
    return 0;    // Non-zero return means that the module couldn't be loaded.
}

static void __exit hello_OS_cleanup(void) { // cleanup function for hello_os module
    printk(KERN_INFO "GoodBye OS! You've removed our module from your kernel! :(( \n");
}

module_init(hello_OS_init);     // define init funciton for module
module_exit(hello_OS_cleanup);  // define cleanup function for module
